# Py3-FAST-API-w-env-Docker
> This is a boilerplate project that helps you get started 


## How to use?
1. Clone or download this project.
```CMD
git clone https://gitlab.com/sr335a__templates/py3-fast-api-w-env-docker.git myProject_001
cd myProject_001
```
2. Use the following command to set the python env 
```cmd
python3 -m venv .py-env
```
3. Once the `.py-env` is created we can activate our env
```CMD
./.py-env/Scripts/activate
```
4. Now download the packages required
```
pip install -r requirements.txt
``` 

Now your environment is set to get your development started!

## Where can I start coding?
> The _main.py_ file under the `app` folder is file that hosts the FastAPI routes and application logic!

## How do I deploy docker and run?
1. Use the following command to build the app into docker image.
```
docker build -t myProject_001__image .
```
2. Use the following command to run the app in from the created docker image!
```
docker run -d --name myProject_001__container_01 -p 80:80 myProject_001__image
```
3. Test access to your running app. Open the browser and check the sample URL
- http://192.168.99.100/items/5?q=somequery 
- - use the appropriate ip per your docker env


